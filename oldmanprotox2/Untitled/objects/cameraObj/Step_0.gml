/// @description Uodate camera

// Update destination
if (instance_exists(follow))
{
	xTo = follow.x;
	yTo = follow.y;
	

}

// Update object position
x += (xTo - x) / 25;
y += (yTo - y) / 25;

// update camera view
camera_set_view_pos(cam, x - viewWHeight, y - viewHHeight);