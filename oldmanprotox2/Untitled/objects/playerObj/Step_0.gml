key_left = keyboard_check(vk_left);
key_right = keyboard_check(vk_right);
key_jump = keyboard_check_pressed(vk_space);

if (mouse_check_button(mb_left))
{
	if (mouse_x < room_width/2)
	{
		key_left = 1;
	} else
	{
		key_right = 1;
	}
	
	if (mouse_y < room_height/2)
	{
		key_jump = 1
	}
	//var xx = mouse_x;
	
}
	
    //var xx = mouse_x;
    //var yy = mouse_y;

//if (mouse_y < width/2)
//{
//	key_left = true;
//}

var move = key_right - key_left;

hsp = move * walksp;
vsp = vsp + grv;

if ((place_meeting(x, y+1, wallObj) && key_jump))
{
	vsp = -8.5;
}

// horizontal collision
if (place_meeting(x+hsp, y, wallObj))
{
	while (!place_meeting(x+sign(hsp), y, wallObj))
	{
		x = x + sign(hsp);
	}
	hsp = 0;
}
x = x + hsp;

// Vertical collision
if (place_meeting(x, y+vsp, wallObj))
{
	while (!place_meeting(x, y+sign(vsp), wallObj))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}
y = y + vsp;

// Animations
if (!place_meeting(x, y+1, wallObj))
{
		sprite_index = playerAirSpr;

	image_speed = 0;
	if (sign(vsp) > 0)
	{
		image_index = 1;
	} else
	{
		image_index = 0;
	}
}
else
{
	image_speed = 1;
	if (hsp == 0)
	{
		sprite_index = playerStandingSpr;
	}
	else
	{
		sprite_index = playerRunningSpr;
	}
}

if (hsp != 0) image_xscale = sign(hsp)